# Speech Emotion Classification 
Build by Ho Tuan Vu - Acoustic Information Science Lab  
Japan Advance Institute of Science and Technology  
Email: tuanvu.ho@jaist.ac.jp  

## Introduction
Source code for speech emotion recognition. This project is based on the top 2 project in ZaloAi Voice Accent & Gender Recognition Challenge 2018.  
The key of this system is based on the assumption that the prosody of speech is a significant cue to emotion recognition. Therefore, the network architecture is designed in the way that it can capture both short-term and long-term speech pattern using CNN layer with different dilation rate. In addition, residual and skip connection to parallelize the processing path, enhancing the system performance.  
### System Overview
The system contains 3 main parts: preprocessing, training and testing.  
![Alt text](imgs/system_overview.png)  

#### Preprocessing
-	Remove frame with energy < threshold  
-	Extracting features using window step = 5ms  
-	Segment long samples in to 3s length  
-	Wrap-padding short samples to 3s length  

### Acoustic features 
-	26 gammatone cepstral coefficients: Response from 128 gammatone filters -> cubic root -> 26-DCT transform  
-	Fundamental frequency F0 extracted using WORLD  
-	Root-mean-square energy RMSE  
-	Delta and delta-delta features  
The gammatone cepstral coefficients shows better performance than MFCC in overall.  

### Training
-	10-fold stratified-split training: ensure the same distribution between each fold  
-	Training 10 models in 200 epochs  
-	Optimizer = Adam  
-	Store the model with best accuracy of each fold  

### Network Architecture
The network architecture is based on the Wavenet architecture [1]. The network contains 4 consecutive dilated CNN blocks. Each block contain 5 dilated CNN cells with different dilation rate (1, 2, 4, 8, 16).  
![Alt text](imgs/network.png)  

### Benchmark
EmoDB is used to train and validate the system. The un-norm and normalized confusion matrix using 10-fold cross-validation is shown below. The overall accuracy is around 0.874.
![Alt text](imgs/confusion_mat.png)   
![Alt text](imgs/confusion_mat_norm.png)

[1] Aaron van den Oord et al: "WAVENET: A GENERATIVE MODEL FOR RAW AUDIO," https://arxiv.org/pdf/1609.03499.pdf

